"use strict";

const message = document.createElement("span");
const exit = document.createElement("span");
exit.textContent = `X`;
exit.style.cursor = "pointer";

inputPrice.addEventListener("focus", () => {
  inputPrice.style.outline = "none";
  inputPrice.style.border = "2px solid green";
});

inputPrice.addEventListener("blur", () => {
  if (inputPrice.value) {
    if (inputPrice.value < 0) {
      inputPrice.style.border = "2px solid red";
      message.textContent = "enter correct price";
      document.querySelector(".wrapper").after(message);
      inputPrice.value = "";
    } else {
      inputPrice.style.border = "";
      inputPrice.style.color = "green";
      message.textContent = `Current price: ${inputPrice.value}`;
      document.querySelector(".wrapper").before(message);

      document.querySelector("span").append(exit);
    }
  } else {
    inputPrice.style.border = "";
  }
});

exit.addEventListener("click", () => {
  message.remove();
  inputPrice.value = "";
});
